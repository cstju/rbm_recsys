# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-04-24 21:10:54
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-04-26 22:37:36
import numpy as np
weights = np.random.randn(6, 2)
data = np.array([[1,1,1,0,0,0],[1,0,1,0,0,0],[1,1,1,0,0,0],[0,0,1,1,1,0], [0,0,1,1,0,0],[0,0,1,1,1,0]])
data = np.insert(data, 0, 1, axis = 1)
print weights
weights = np.insert(weights, 0, 0,axis = 1)
print weights
weights = np.insert(weights, 0, 0,axis = 0)
print weights
print data
pos_hidden_activations = np.dot(data, weights)
print 'pos_hidden_activations'
print pos_hidden_activations
pos_hidden_probs = 1.0 / (1 + np.exp(-pos_hidden_activations))
print pos_hidden_probs
pose_hidden_random = np.random.rand(6, 3)
print pose_hidden_random
pos_hidden_states = pos_hidden_probs > pose_hidden_random
print pos_hidden_states
pos_associations = np.dot(data.T, pos_hidden_probs)
print pos_associations
neg_visible_activations = np.dot(pos_hidden_states, weights.T)
print neg_visible_activations
neg_visible_activations[:,0] = 1 # Fix the bias unit.
print neg_visible_activations