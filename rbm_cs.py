# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-04-03 22:43:53
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-05-09 10:54:49
import numpy as np
import scipy as sp
import pdb

from numpy.random import normal,random,uniform
'''
   this code still have some problem,I can only get 0.98 rmse on movielens data
   If you can figure it out,PLEASE!!! tell me . 
'''
class TEMP:
	def __init__(self):
		self.AccVH=None
		self.CountVH=None
		self.AccV=None
		self.CountV=None
		self.AccH=None
class  CF_RMB:
	def __init__(self,X,UserNum=943,HiddenNum=30,ItemNum=1682,Rate=5):
		self.X=np.array(X)
		self.HiddenNum=HiddenNum
		self.ItemNum=ItemNum
		self.UserNum=UserNum
		self.Rate=Rate
		self.movie_user={}
		self.user_movie={}
		
		########################################################
		#初始化输入元偏置的另一种方法
		self.bik=normal(0,0.01,(self.ItemNum,self.Rate))
		########################################################
	
		#bik是输入元偏置，bik的类型是数组
		#self.bik=np.zeros((self.ItemNum,self.Rate))
		self.Momentum={}

		self.Momentum['bik']=np.zeros((self.ItemNum,self.Rate))

		self.UMatrix=np.zeros((self.UserNum,self.ItemNum))

		self.V=np.zeros((self.ItemNum,self.Rate))

		for i in range(self.X.shape[0]):
			#对于将uid，mid，rat做减1处理的原因是：程序后面要用它们做索引
			#但是对于矩阵索引都是从0开始，为了匹配，将它们减1
			uid=self.X[i][0]-1#减1是为了让用户id从0开始到942
			mid=self.X[i][1]-1#减1是为了让商品id从0开始到1681
			rat=self.X[i][2]-1#减1是为了让评分从0开始到4
			self.UMatrix[uid][mid]=1#用户-电影矩阵，评分过就将值设为1
			self.bik[mid][rat]+=1#构建电影流行度矩阵，每次对某电影评分就加1
			self.movie_user.setdefault(mid,{})
			self.user_movie.setdefault(uid,{})
			self.movie_user[mid][uid]=rat#构建电影-用户评分矩阵
			self.user_movie[uid][mid]=rat#构建用户-电影评分矩阵
		pass
		#构建三维（商品数x评分数x隐藏元个数）权重
		self.W=normal(0,0.01,(self.ItemNum,self.Rate,HiddenNum))
		self.Momentum['W']=np.zeros(self.W.shape)
		#self.initialize_bik()#初始化输入元偏置
		#构建隐藏元偏置，并将其转化为一个一维行向量
		self.bj=np.zeros(HiddenNum)
		self.Momentum['bj']=np.zeros(HiddenNum)
		self.Dij=np.zeros((self.ItemNum,self.HiddenNum))
		self.Momentum['Dij']=np.zeros((self.ItemNum,self.HiddenNum))
	"""
	def initialize_bik(self):
		for i in range(self.ItemNum):
			total=np.sum(self.bik[i])
			if total>0:
				for k in range(self.Rate):
					if self.bik[i][k]==0:
						self.bik[i][k]=-10
					else:
						self.bik[i][k]=np.log(self.bik[i][k]/total)
	"""


	def clamp_user(self,uid):
		"""
		V矩阵的行列是：电影数x评分数（0-4）
		对于某一具体用户，遍历评分，将对应电影的对应评分设为1，其余为0
		返回的V是一个(电影总数x评分等级)的矩阵
		"""
		V=np.zeros(self.V.shape)
		for i in self.user_movie[uid]:
			V[i][self.user_movie[uid][i]]=1
		#print V.shape
		#pdb.set_trace()
		return V

	def update_hidden(self,V,uid):
		"""
		更新隐藏元状态，当给定输入元状态时，隐藏元状态为1的概率
		"""
		"""
		这里这个r和后面计算pj时“np.dot(r,self.Dij)”是仿照论文中做的内容，
		r矩阵代表用户看过哪些电影，但实际上没有评分，Netflix数据集中有此内容
		论文中采用的是Netflix数据集，并将这个作为一个隐性信息用来增加准确性，
		但是编代码的人使用的是Movielens数据集，没有这个隐藏信息，
		所以我将代码修改回不增加隐性信息的情况
		得到的pj是一个（隐藏元个数x1）的矩阵
		"""
		r=self.UMatrix[uid]
		hp=None
		for i in self.user_movie[uid]:
			if hp==None:
				"""
				V[i]是指对于某一固定用户，电影i的评分矩阵（1x5），例如
				[ 0.  0.  1.  0.  0.] 这就是一个V[i],表示评分为3
				W[i]是指对于某一固定电影，与隐藏元相连接的权重矩阵（5x30）
				V[i]xW[i]得到1x30的矩阵，代表一个具体的电影对应的30隐藏元值
				"""
				hp=np.dot(V[i],self.W[i]).flatten(1)
			else:
				#将该固定用户的所有评分电影对30个隐藏元相累加
				hp+=np.dot(V[i],self.W[i]).flatten(1)
		#pdb.set_trace()
		bj_hp = self.bj+hp
		#pj=1/(1+np.exp(-self.bj-hp+np.dot(r,self.Dij).flatten(1)))
		pj=1/(1+np.exp(-bj_hp))#bj代表隐藏元偏置，pj代表隐藏元为1概率
		return pj

		
	def sample_hidden(self,pj):
		"""
		sample_hidden函数的作用是将得到的隐藏神经元的值转化为0或1
		但是并不是四舍五入转化为0-1，而是与一组随机数比较
		"""

		sh=uniform(size=pj.shape)#sh初始化为与pj个数相同的随机数，取值在0-1之间
		for i in range(sh.shape[0]):
			if sh[i]<pj[i]:
				sh[i]=1.0
			else:
				sh[i]=0.0
		return sh

	def update_visible(self,sh,uid,mid=None):
		"""
		update_visible函数是根据隐藏神经元状态sh（已做0-1化处理）
		来回推输入层的神经元状态
		"""
		if mid==None:
			vp=np.zeros(self.V.shape)#vp初始化为一个全0矩阵（所有电影书x评分等级）
			for i in self.user_movie[uid]:#对于固定用户uid遍历他看过的所有电影
				"""
				根据公式计算概率，其中self.bik[i]为电影i对应的各个评分等级偏置，
				这里self.bik[i]为一个（1x5）的矩阵
				np.dot(self.W[i],sh)为某一电影的输入层与隐藏层的连接权重W[i]（5x30）
				与0-1化的隐藏层状态sh(30x1)
				"""
				#print self.W[i].shape
				#print type(self.W[i])
				#print sh.shape
				#print type(sh)
				#print sh
				#pdb.set_trace() 
				vp[i]=np.exp(self.bik[i]+np.dot(self.W[i],sh))
				vp[i]=vp[i]/np.sum(vp[i])
				#print sh
				#pdb.set_trace()
			return vp
		vp=np.exp(self.bik[mid]+np.dot(self.W[mid],sh))
		vp=vp/np.sum(vp)
		return vp

	def sample_visible(self,vp,uid):

		"""
		将得到的vp概率形式转化为0-1值
		"""
		sv=uniform(size=vp.shape)
		#print sv.shape
		#pdb.set_trace()
		for i in range(sv.shape[0]):
			for j in range(sv.shape[1]):
				if sv[i][j]<vp[i][j]:
					sv[i][j]=1.0
				else:
					sv[i][j]=1.0
		return sv
		"""
		sv=np.zeros(self.V.shape)
		for i in self.user_movie[uid]:
			r=uniform()
			k=0
			for k in range(self.Rate):
				r-=vp[i][k]
				if r<=0:break
			sv[i][k]=1
		return sv 
		"""


	def accum_temp(self,V,pj,temp,uid):
		for i in self.user_movie[uid]:
			"""
			V[i].reshape(-1,1)得到的是电影i的一个列向量（5x1）
			pj.reshape(1,-1)得到的是隐藏元的一个行向量（1x30）
			temp.AccVH[i]得到的是（5x30）的矩阵
			每一次调用函数accum_temp都会累加temp.AccVH[i]和temp.CountVH[i]
			"""
			temp.AccVH[i]+=np.dot(V[i].reshape(-1,1),pj.reshape(1,-1))
			temp.CountVH[i]+=1#temp.CountVH[i]是一个5x30的矩阵，每次都让矩阵中每个元素加1
			temp.AccV[i]+=V[i]#每次调用accum_temp函数都会让某一用户的评分电影i对应的V[i]累加到temp.AccV[i]
			temp.CountV[i]+=1#矩阵CountV[i]是一个(1x5)的矩阵，每次调用accum_temp函数都会让某一用户的评分电影i对应的CountV[i]累加1
		temp.AccH+=pj#temp.AccH初始化是全0矩阵(1x30)每次累加隐藏元的值
		return temp

	def deaccum_temp(self,V,pj,temp,uid):
		"""
		V[i].reshape(-1,1)得到的是电影i的一个列向量（5x1）
		pj.reshape(1,-1)得到的是隐藏元的一个行向量（1x30）
		temp.AccVH[i]得到的是（5x30）的矩阵
		pj.reshape(1,-1)得到的是隐藏元的一个行向量（1x30）
		每一次调用函数deaccum_temp都会累减temp.AccVH[i]

		"""
		for i in self.user_movie[uid]:
			temp.AccVH[i]-=np.dot(V[i].reshape(-1,1),pj.reshape(1,-1))
			temp.AccV[i]-=V[i]#每次调用deaccum_temp函数都会让某一用户的评分电影i对应的V[i]累减到temp.AccV[i]
		temp.AccH-=pj
		return temp	

	def updateall(self,temp,batch_size,para,watched):
		delatW=np.zeros(temp.CountVH.shape)
		delatBik=np.zeros(temp.CountV.shape)
	
		delatW[temp.CountVH!=0]=temp.AccVH[temp.CountVH!=0]/temp.CountVH[temp.CountVH!=0]
		delatBik[temp.CountV!=0]=temp.AccV[temp.CountV!=0]/temp.CountV[temp.CountV!=0]
		delataBj=temp.AccH/batch_size

		self.Momentum['W'][temp.CountVH!=0]=self.Momentum['W'][temp.CountVH!=0]*para['Momentum']
		self.Momentum['W'][temp.CountVH!=0]+=para['W']*(delatW[temp.CountVH!=0]-para['weight_cost']*self.W[temp.CountVH!=0])
		self.W[temp.CountVH!=0]+=self.Momentum['W'][temp.CountVH!=0]

		self.Momentum['bik'][temp.CountV!=0]=self.Momentum['bik'][temp.CountV!=0]*para['Momentum']
		self.Momentum['bik'][temp.CountV!=0]+=para['bik']*delatBik[temp.CountV!=0]
		self.bik[temp.CountV!=0]+=self.Momentum['bik'][temp.CountV!=0]

		self.Momentum['bj']=self.Momentum['bj']*para['Momentum']
		self.Momentum['bj']+=para['bj']*delataBj
		self.bj+=self.Momentum['bj']

		for i in range(self.ItemNum):
			if watched[i]==1:
				self.Momentum['Dij'][i]=self.Momentum['Dij'][i]*para['Momentum']
				self.Momentum['Dij'][i]+=para['D']*temp.AccH/batch_size
				self.Dij[i]+=self.Momentum['Dij'][i]
		
		np.seterr(all='raise')

	def train(self,para,test_X,cd_steps=3,batch_size=30,numEpoch=100,Err=0.00001):
		for epo in range(numEpoch):
			print 'the ',epo,'-th  epoch is running'
			#生成一个一维数组kk值是用户数的随机排列
			kk=np.random.permutation(range(self.UserNum))
			bt_count=0
			while bt_count<=self.UserNum:
				#batch_size表示一次处理的用户个数，这里为30代表将全部用户集分成小块
				#每块是30个用户，然后一块运算完之后再选取下一块的30个用户
				btend=min(self.UserNum,bt_count+batch_size)
				#这里的users为一个块，随机选取的30个用户
				users=kk[bt_count:btend]
				temp=TEMP#声明TEMP类的对象temp
				temp.AccVH=np.zeros(self.W.shape)#建立电影数x评分数x隐藏元个数的全0矩阵
				temp.CountVH=np.zeros(self.W.shape)#建立电影数x评分数x隐藏元个数的全0矩阵
				temp.AccV=np.zeros(self.V.shape)#建立电影数x评分数的全0矩阵
				temp.CountV=np.zeros(self.V.shape)#建立电影数x评分数的全0矩阵
				temp.AccH=np.zeros(self.bj.shape)#建立隐藏元个数全0矩阵
				#watched=np.zeros(self.UMatrix[0].shape)
				watched=np.zeros(self.ItemNum)
				for user in users:
					#对于某一用户，如果他评分过某个电影就将watch中对应的电影编号的值设为1，
					#没有评分过的电影，值还是初始的0
					watched[self.UMatrix[user]==1]=1

					sv=self.clamp_user(user)#sv（电影总数x评分等级）
					#pj是一个（隐藏元个数x1）的矩阵
					pj=self.update_hidden(sv,user)#pj隐藏层状态是以用户为单位，一个用户对应一个pj矩阵
					#print 'pj的规模：',pj.shape
					#pdb.set_trace()
					temp=self.accum_temp(sv,pj,temp,user)
					#AccVH+=pj*
					for step in range(cd_steps):
						sh=self.sample_hidden(pj)#sh隐藏层状态是以用户为单位，一个用户对应一个sh矩阵
						vp=self.update_visible(sh,user)
						sv=self.sample_visible(vp,user)
						pj=self.update_hidden(sv,user)
					deaccum_temp=self.deaccum_temp(sv,pj,temp,user)
				self.updateall(temp,batch_size,para,watched)	
				#updateall============================================	
				bt_count+=batch_size
			self.test(test_X)


	def pred(self,uid,mid):
		V=self.clamp_user(uid)
		pj=self.update_hidden(V,uid)
		vp=self.update_visible(pj,uid,mid)
		ans=0
		for i in range(self.Rate):
			ans+=vp[i]*(i+1)
		return ans
	 
	def test(self,test_X):
		output=[]
		sums=0
		test_X=np.array(test_X)
		#print "the test data size is ",test_X.shape
		for i in range(test_X.shape[0]):
			pre=self.pred(test_X[i][0]-1,test_X[i][1]-1)
			#print test_X[i][2],pre
			output.append(pre)
			#print pre,test_X[i][2]
			sums+=(pre-test_X[i][2])**2
		rmse=np.sqrt(sums/test_X.shape[0])
		print "the rmse on test data is ",rmse
		return output

def read_data():
	train=open('/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.base').read().splitlines()
	test=open('/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.test').read().splitlines()
	train_X=[]
	test_X=[]
	for line in train:
		p=line.split('	');
		train_X.append([int(p[0]),int(p[1]),int(p[2])])
	for line in test:
		p=line.split('	');
		test_X.append([int(p[0]),int(p[1]),int(p[2])])
	return train_X,test_X
train_X,test_X=read_data()
print np.array(train_X).shape,np.array(test_X).shape
a=CF_RMB(train_X)
para={}
para['W']=0.005
para['bik']=0.005
para['bj']=0.05
para['D']=0.001
para['weight_cost']=0.001
para['Momentum']=0.7
para['Anneal']=3
a.train(para,test_X)
a.test(test_X)